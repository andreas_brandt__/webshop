﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Controllers.WebShopControllers;
using WebShop.Models.ShopModels;
using WebShop.BLL;
using WebShop.Services;

namespace WebShop.Test
{
    public static class ProductsControllerFactory
    {
        public static ProductsController GetProductsController(WebShopRepository context)
        {
            var fixerIO = new FixerIOMock();
            ProductBL productBL = new ProductBL(context, fixerIO);
            return new ProductsController(productBL, fixerIO);
        }
    }
}
