﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Controllers.WebShopControllers;
using WebShop.Models.ShopModels;
using WebShop.Test;
using WebShop.ViewModels;
using Xunit;

namespace WebShop
{
    public class ProductsControllerTest
    {
        public ProductsControllerTest()
        {

        }
        private static DbContextOptions<WebShopRepository> CreateNewContextOptions()
        {
            // Create a fresh service provider, and therefore a fresh 
            // InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<WebShopRepository>();
            builder.UseInMemoryDatabase()
                   .UseInternalServiceProvider(serviceProvider);

            //serviceProvider.
            return builder.Options;
        }


        [Fact]
        public async Task IndexListAllProducts()
        {
            //Arrange
            // All contexts that share the same service provider will share the same InMemory database
            var options = CreateNewContextOptions();
            CreateTestData(options);
            

            // Use a clean instance of the context to run the test
            using (var context = new WebShopRepository(options))
            {
                var c = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
                var productsController = ProductsControllerFactory.GetProductsController(context);

                //Act
                var result = await productsController.Index(string.Empty);

                //Assert
                var viewResult = Assert.IsType<ViewResult>(result);
                var model = Assert.IsAssignableFrom<IEnumerable<ProductViewModel>>(
                    viewResult.ViewData.Model);
                Assert.Equal(2, model.Count());
                Assert.Equal("TestProduct 1", model.ElementAt(0).ProductName);
            }
        }

        [Fact]
        public async Task CreateAddsProduct()
        {
            //Arrange
            var options = CreateNewContextOptions();

            var productToCreate =
                new ProductViewModel
                {
                    ProductCategoryId = 1,
                    ProductDescription = "Test description",
                    ProductName = "Test ProductName",
                    ProductPrice = 99.5m,
                    ProductCategory = new ProductCategory { ProductCategoryId = 0, ProductCategoryName = "Test"},
                    ProductId = 0
                };

            using (var context = new WebShopRepository(options))
            {
                var productsController = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await productsController.Create(productToCreate);

                // Assert
                Assert.IsType<RedirectToActionResult>(result);
                Assert.Equal(1, context.Products.Count());
                //Assert.Equal<ProductViewModel>(productToCreate, context.Products.First());
            }
        }

        [Fact]
        public async Task CreateReturnsViewResultWithInvalidProductWhenInvalidProduct()
        {
            var options = CreateNewContextOptions();
            var productToCreate =
                new ProductViewModel
                {
                    ProductCategoryId = 1,
                    ProductName ="TestCreateProduct 1",
                    ProductPrice = -99.5m,
                    ProductCategory = new ProductCategory { ProductCategoryId = 0, ProductCategoryName = "Test" },
                    ProductId = 0
                };

            using (var context = new WebShopRepository(options))
            {
                var productsController = ProductsControllerFactory.GetProductsController(context);
                productsController.ModelState.AddModelError("test", "test");
                
                // Act
                var result = await productsController.Create(productToCreate);

                // Assert
                var viewResult = Assert.IsType<ViewResult>(result);
                var model = Assert.IsAssignableFrom<ProductViewModel>(viewResult.ViewData.Model);
                Assert.Equal(0, context.Products.Count());
                Assert.Equal(productToCreate, model);
            }
        }


        [Fact]
        public async Task EditReturnsNotFoundWhenIdIsNull()
        {
            // Arrange

            var options = CreateNewContextOptions();

            using (var context = new WebShopRepository(options))
            {
                var productsController = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await productsController.Edit(null);

                // Assert
                Assert.IsType<NotFoundResult>(result);
            }
        }

        [Fact]
        public async Task EditReturnsNotFoundWhenIdNotFound()
        {
            var options = CreateNewContextOptions();
            using (var context = new WebShopRepository(options))
            {
                var productsController = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await productsController.Edit(1);

                // Assert
                Assert.IsType<NotFoundResult>(result);
            }
        }

        [Fact]
        public async Task EditReturnsViewResultWithProductWhenIdFound()
        {
            // Arrange
            var options = CreateNewContextOptions();
            CreateTestData(options);

            using (var context = new WebShopRepository(options))
            {
                var productsController = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await productsController.Edit(1);

                var viewResult = Assert.IsType<ViewResult>(result);
                var model = Assert.IsAssignableFrom<ProductViewModel>(viewResult.ViewData.Model);
                Assert.Equal(context.Products.SingleOrDefault(x => x.ProductId == 1).ProductId, model.ProductId);
                
            }
        }

        [Fact]
        public async Task IndexReturnsSearchResult()
        {
            // Arrange
            var options = CreateNewContextOptions();
            CreateTestData(options);

            using (var context = new WebShopRepository(options))
            {
                var productsController = ProductsControllerFactory.GetProductsController(context);

                //Act
                // Case insensitive
                var result = await productsController.Index("tEsT");

                //Assert
                var viewResult = Assert.IsType<ViewResult>(result);
                var model = Assert.IsAssignableFrom<IEnumerable<ProductViewModel>>(
                    viewResult.ViewData.Model);

                Assert.Equal(2, model.Count());
                Assert.Equal("TestProduct 1", model.ElementAt(0).ProductName);
                Assert.Equal("TestProduct 2", model.ElementAt(1).ProductName);
            }
        }

        [Fact]
        public async Task DetailsReturnsViewModelWithParameterId()
        {
            // Arrange
            var options = CreateNewContextOptions();
            CreateTestData(options);


            using (var context = new WebShopRepository(options))
            {
                var controller = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await controller.Details(1);

                // Assert
                var viewResult = Assert.IsType<ViewResult>(result);
                var model = Assert.IsAssignableFrom<ProductViewModel>(
                    viewResult.ViewData.Model);
                Assert.Equal(model.ProductName, "TestProduct 1");
            }
        }

        [Fact]
        public async Task DetailsReturnNotFoundWhenNotFound()
        {
            // Arrange
            var options = CreateNewContextOptions();
            CreateTestData(options);


            using (var context = new WebShopRepository(options))
            {
                var controller = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await controller.Details(3);

                // Assert
                var viewResult = Assert.IsType<NotFoundResult>(result);

            }
        }

        [Fact]
        public async Task DetailsReturnNotFoundWhenParameterNull()
        {
            // Arrange
            var options = CreateNewContextOptions();
            CreateTestData(options);


            using (var context = new WebShopRepository(options))
            {
                var controller = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await controller.Details(null);

                // Assert
                var viewResult = Assert.IsType<NotFoundResult>(result);

            }
        }

        [Fact]
        public async Task DeleteReturnsItemWithParameterId()
        {
            // Arrange
            var options = CreateNewContextOptions();
            CreateTestData(options);


            using (var context = new WebShopRepository(options))
            {
                var controller = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await controller.Delete(1);

                // Assert
                var viewResult = Assert.IsType<ViewResult>(result);
                var model = Assert.IsAssignableFrom<ProductViewModel>(
                    viewResult.ViewData.Model);
                Assert.Equal(model.ProductName, "TestProduct 1");
            }
        }
        [Fact]
        public async Task DeleteConfirmedDeletesItemWithParameterId()
        {
            // Arrange
            var options = CreateNewContextOptions();
            CreateTestData(options);

            using (var context = new WebShopRepository(options))
            {
                var controller = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await controller.DeleteConfirmed(1);

                // Assert
                var viewResult = Assert.IsType<RedirectToActionResult>(result);
                Assert.Equal(1, context.Products.Count());
                Assert.Equal(2, context.Products.First().ProductId);
            }
        }

        [Fact]
        public async Task DeleteReturnNotFoundWhenNotFound()
        {
            // Arrange
            var options = CreateNewContextOptions();
            CreateTestData(options);


            using (var context = new WebShopRepository(options))
            {
                var controller = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await controller.Delete(3);

                // Assert
                var viewResult = Assert.IsType<NotFoundResult>(result);

            }
        }

        [Fact]
        public async Task DeleteReturnNotFoundWhenParameterNull()
        {
            // Arrange
            var options = CreateNewContextOptions();
            CreateTestData(options);


            using (var context = new WebShopRepository(options))
            {
                var controller = ProductsControllerFactory.GetProductsController(context);

                // Act
                var result = await controller.Delete(null);

                // Assert
                var viewResult = Assert.IsType<NotFoundResult>(result);

            }
        }
        private void CreateTestData(DbContextOptions<WebShopRepository> options)
        {
            using (var context = new WebShopRepository(options))
            {
                context.ProductCategories.Add(new ProductCategory
                {
                    ProductCategoryName = "TestCategory 1",
                });
                context.SaveChanges();

                context.Products.Add(new Product
                {
                    Translations =
                    new List<ProductTranslation>
                    {
                        new ProductTranslation { Language = "sv", ProductName = "TestProduct 1" }
                    },
                    ProductPrice = 12.3m,
                    ProductCategory = context.ProductCategories.First()
                });

                context.Products.Add(new Product
                {
                    Translations =
                    new List<ProductTranslation>
                    {
                        new ProductTranslation { Language = "sv", ProductName = "TestProduct 2" }
                    },
                    ProductPrice = 12.3m,
                    ProductCategory = context.ProductCategories.First()

                });
                context.SaveChanges();
            }
        }
        //[Fact]
        //public async Task TestIndexReturnsListProvided()
        //{
        //    WebShopRepository repo = new WebShopRepository(new DbContextOptions<WebShopRepository>());
        //    ProductsController p = new ProductsController(repo);
        //    var result = await p.Index();
        //}
    }
}
