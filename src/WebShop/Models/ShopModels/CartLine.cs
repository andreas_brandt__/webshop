﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebShop.Models.ShopModels
{
    /// <summary>
    /// Model for DB
    /// </summary>
    public class CartLine
    {
        public int CartLineId { get; set; }
        public string CartId { get; set; }
        public int Quantity { get; set; }

        [Required]
        public Product Product { get; set; }
        [Required]
        public int ProductId { get; set; }
    }
}
