﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
//using WebShop.Resources.Localization.Models.ShopModels;

namespace WebShop.Models.ShopModels
{
    public class Product
    {
        public int ProductId { get; set; }

        [DataType(DataType.Currency)]
        [Range(0.1, 1000000)]
        [Required]
        public decimal ProductPrice { get; set; }

        //[Required]
        //[DataType(DataType.Text)]
        //public string ProductItemNumber { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ProductImageUrl { get; set; }

        // Foreign key
        public int ProductCategoryId { get; set; }
        public ProductCategory ProductCategory { get; set; }

        [Required]
        public virtual ICollection<ProductTranslation> Translations { get; set; }

    }
}
