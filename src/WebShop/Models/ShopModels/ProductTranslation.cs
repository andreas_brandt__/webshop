﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebShop.Models.ShopModels
{
    public class ProductTranslation
    {
        [JsonIgnore]
        public int ProductId { get; set; }
        public string Language { get; set; }


        [Display(Name = "ProductName", ResourceType = typeof(WebShop.Resources.Localization.Models.ShopModels.Product))] // , ResourceType = typeof(Resources.Localization.Models.ShopModels.Product)
        [MaxLength(30, ErrorMessage = "Max 30 characters long name")]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Product name is required")]
        public string ProductName { get; set; }

        [Display(Name = "Description", ResourceType = typeof(WebShop.Resources.Localization.Models.ShopModels.Product))]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[a-zåäöA-ZÅÄÖ\s]{1,40}$", ErrorMessage = "Only characters are allowed")]
        public string ProductDescription { get; set; }
    }
}
