﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebShop.Models.ShopModels
{
    public class ProductCategory
    {
        public int ProductCategoryId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        [Display(Name = "Category", ResourceType = typeof(WebShop.Resources.Localization.Models.ShopModels.ProductCategory))]
        public string ProductCategoryName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Category description")]
        public string ProductCategoryDescription { get; set; }


        // Ignore in Json to prevent self reference
        [JsonIgnore]
        public ICollection<Product> Products { get; set; }
    }
}
