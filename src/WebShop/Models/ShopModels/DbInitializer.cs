﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebShop.Models.ShopModels
{
    public static class DbInitializer
    {
        public static void Initialize(WebShopRepository context)
        {
            context.Database.EnsureCreated();

            if (context.Products.Any())
            {
                return;
            }

            var productCategories = new ProductCategory[]
            {
                new ProductCategory { ProductCategoryName = "Djur", ProductCategoryDescription="Diverse djur", Products = new List<Product>() },
                new ProductCategory { ProductCategoryName = "Hårdvara", ProductCategoryDescription = "Grejer", Products = new List<Product>() },
                new ProductCategory { ProductCategoryName = "Annat", ProductCategoryDescription = "Helt andra grejer", Products = new List<Product>()}
            };

            context.ProductCategories.RemoveRange(context.ProductCategories);
            context.SaveChanges();
            foreach (var category in productCategories) context.Add(category);
            context.SaveChanges();




            var products = new Product[]
            {
                new Product
                {
                    ProductCategory = productCategories[0],
                    ProductPrice = 100m,
                    ProductImageUrl = "http://d39kbiy71leyho.cloudfront.net/wp-content/uploads/2016/05/09170020/cats-politics-TN.jpg",
                    Translations = new List<ProductTranslation>()
                },
                new Product
                {
                    ProductCategory = productCategories[0],
                    ProductPrice = 12.2m,
                    ProductImageUrl = "https://cdn.pixabay.com/photo/2014/04/13/20/49/cat-323262_960_720.jpg",
                    Translations = new List<ProductTranslation>()
                },
                new Product
                {
                    ProductCategory = productCategories[0],
                    ProductPrice = 1030m,
                    ProductImageUrl = "http://www.life.org.uk/dump/media/animal-adaptation-animal-adaptation-800-360.jpg",
                    Translations = new List<ProductTranslation>()
                },
                new Product
                {
                    ProductCategory = productCategories[0],
                    ProductPrice = 100000m,
                    ProductImageUrl = "http://geology.com/stories/13/bear-areas/grizzly-bear.jpg",
                    Translations = new List<ProductTranslation>()
                }
            };
            foreach (var product in products) context.Add(product);
            context.SaveChanges();

            var productTranslations = new ProductTranslation[]
            {
                new ProductTranslation { Language = "sv", ProductId = products[0].ProductId, ProductName = "Välklädd katt", ProductDescription = "Väldigt välklädd katt" },
                new ProductTranslation { Language = "en", ProductId = products[0].ProductId, ProductName = "Well dressed cat", ProductDescription = "Very well dressed cat" },
                new ProductTranslation { Language = "sv", ProductId = products[1].ProductId, ProductName = "Vanlig katt", ProductDescription = "En helt vanlig katt" },
                new ProductTranslation { Language = "en", ProductId = products[1].ProductId, ProductName = "Ordinary catt", ProductDescription = "Completely ordinary cat" },
                new ProductTranslation { Language = "sv", ProductId = products[2].ProductId, ProductName = "Groda", ProductDescription = "En helt vanlig groda" },
                new ProductTranslation { Language = "en", ProductId = products[2].ProductId, ProductName = "Frog", ProductDescription = "Completely ordinary frog" },
                new ProductTranslation { Language = "sv", ProductId = products[3].ProductId, ProductName = "Björn", ProductDescription = "En helt vanlig björn" },
                new ProductTranslation { Language = "en", ProductId = products[3].ProductId, ProductName = "Bear", ProductDescription = "Completely ordinary bear" }
            };
            context.ProductTranslations.RemoveRange(context.ProductTranslations);
            context.SaveChanges();
            foreach (var translation in productTranslations) context.Add(translation);
            context.SaveChanges();



        }
    }
}
