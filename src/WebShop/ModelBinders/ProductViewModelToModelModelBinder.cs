﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models.ShopModels;
using WebShop.ViewModels;

namespace WebShop.ModelBinders
{
    public class ProductViewModelToModelModelBinder : IModelBinder
    {
        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
                throw new ArgumentNullException(nameof(bindingContext));

            var repo = bindingContext.HttpContext.RequestServices.GetService(
                typeof(WebShopRepository)) as WebShopRepository;

            
            if (bindingContext.ModelType == typeof(ProductViewModel))
            {
                ProductViewModel viewModel = (ProductViewModel)bindingContext.Model;

                Product product =
                    await repo.Products.SingleOrDefaultAsync(
                        p => p.ProductId == viewModel.ProductId);

                product.ProductCategory = viewModel.ProductCategory;
                product.ProductCategoryId = viewModel.ProductCategoryId;
                product.ProductImageUrl = viewModel.ProductImageUrl;
                product.ProductPrice = viewModel.ProductPrice;

                var translation = await repo.ProductTranslations.SingleOrDefaultAsync(
                    t =>
                    t.Language == CultureInfo.CurrentCulture.TwoLetterISOLanguageName
                    && t.ProductId == viewModel.ProductId);

                var translations =
                    repo.ProductTranslations
                    .Where(p => p.ProductId == viewModel.ProductId).ToList();


                if (translation == null)
                {
                    translations.Add(
                        new ProductTranslation
                        {
                            Language = CultureInfo.CurrentCulture.TwoLetterISOLanguageName,
                            ProductDescription = viewModel.ProductDescription,
                            ProductId = viewModel.ProductId,
                            ProductName = viewModel.ProductName
                        });
                }

                product.Translations = translations;

                bindingContext.Result = ModelBindingResult.Success(product);
            }
            else
            {
                bindingContext.Result = ModelBindingResult.Failed();

            }
        }
    }
}
