﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using WebShop.BLL;
using WebShop.ModelBinders;
using WebShop.Models.ShopModels;
using WebShop.ViewModels;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using WebShop.Services;

namespace WebShop.Controllers.WebShopControllers
{
    public class CartController : Controller
    {
        private ProductsBusinessLayer _productsBL;
        private KlarnaCheckout _klarnaCheckoutService;
        private IFixerIO _fixerIOService;

        public CartController(ProductsBusinessLayer productsBL, KlarnaCheckout klarnaCheckoutService, IFixerIO fixerIOService)
        {
            _productsBL = productsBL;
            _klarnaCheckoutService = klarnaCheckoutService;
            _fixerIOService = fixerIOService;
        }

        public async Task<IActionResult> Index()
        {
            var cartBL = CartBL.GetCart(this.HttpContext);

            var viewModel = new CartLinesViewModel
            {
                CartLines = await CartLinesViewModelFromModels(await cartBL.GetCartItems()),
                TotalPrice = await cartBL.GetTotal(),
                Quantity = await cartBL.GetCount()
            };
            
            return View(viewModel);
        }

        //Store/AddToCart/5
        public async Task<IActionResult> AddToCart(int id, string returnUrl)
        {
            // Retreive the product from BL
            var addedProduct = await _productsBL.GetItem(id);

            // Add it to the shopping cart
            var cart = CartBL.GetCart(this.HttpContext);

            await cart.AddToCart(addedProduct);

            return Redirect(returnUrl);
        }

        //ShoppingCart/RemoveFromCart/5
        [HttpPost]
        public async Task<IActionResult> RemoveOneFromCart(int id)
        {
            var cartBL = CartBL.GetCart(this.HttpContext);
            await cartBL.RemoveOneFromCart(id);
            return RedirectToAction("Index");
        }
        
        [HttpPost]
        public async Task<IActionResult> EmptyCart(string returnUrl)
        {
            var cart = CartBL.GetCart(this.HttpContext);
            await cart.EmptyCart(this.HttpContext);
            return Redirect(returnUrl);
        }

        public async Task<IActionResult> OrderConfirmedEmptyCart()
        {
            var cart = CartBL.GetCart(this.HttpContext);
            await cart.EmptyCart(this.HttpContext);
            return ViewComponent("Cart");
        }
        

        public async Task<IActionResult> ChangeQuantity(int id, int newQuantity)
        {
            var cart = CartBL.GetCart(this.HttpContext);
            await cart.ChangeQuantity(id, newQuantity);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> RemoveCartLine(int id)
        {
            var cart = CartBL.GetCart(this.HttpContext);
            await cart.ChangeQuantity(id, 0);
            return RedirectToAction("Index");
        }


        [Route("Cart/CheckoutOrder")]
        public async Task<IActionResult> CheckoutOrder()
        {
            var cartBL = CartBL.GetCart(this.HttpContext);
            string snippet = await _klarnaCheckoutService.CheckoutCart(cartBL);
            return View("CheckoutOrder", snippet);
        }

        public async Task<IActionResult> OrderConfirmation(string klarna_order_id)
        {
            string snippet = _klarnaCheckoutService.OrderConfirmation(klarna_order_id);
            
            var cartBL = CartBL.GetCart(this.HttpContext);
            await cartBL.EmptyCart(this.HttpContext);
            return View("OrderConfirmation", snippet);
        }

        public IActionResult KlarnaCallback(string klarna_order_id)
        {
            return View();
        }


        private async Task<List<CartLineViewModel>> CartLinesViewModelFromModels(List<CartLine> cartLines)
        {
            List<CartLineViewModel> viewModelLines = new List<CartLineViewModel>();
            decimal rate = await _fixerIOService.ReadRate();

            foreach (var line in cartLines)
            {
                viewModelLines.Add(new CartLineViewModel
                {
                    CartLine = line,
                    ProductName = _productsBL.GetLocalizedProductName(line.Product),
                    ProductPrice = line.Product.ProductPrice * rate,
                    LinePriceSum = line.Product.ProductPrice * rate * line.Quantity

                });
            }
            return viewModelLines;
        }



        //// GET: /ShoppingCart/CartSummary
        //[ChildActionOnly]
        //public ActionResult CartSummary()
        //{
        //    var cart = ShoppingCart.GetCart(this.HttpContext);

        //    ViewData["CartCount"] = cart.GetCount();
        //    return PartialView("CartSummary");
        //}
    }
}
