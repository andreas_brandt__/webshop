using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebShop.BLL;
using WebShop.ModelBinders;
using WebShop.Models.ShopModels;
using WebShop.Services;
using WebShop.ViewModels;

namespace WebShop.Controllers.WebShopControllers
{
    public class ProductsController : Controller
    {
        private ProductBL _productBL;
        private IFixerIO _fixerIOService;
        private decimal _rate;

        public ProductsController(ProductBL productBL, IFixerIO fixerIOService)
        {
            _productBL = productBL;
            _fixerIOService = fixerIOService;
        }

        // GET: Products
        public async Task<IActionResult> Index(string searchTerm, int? productCategoryId = null)
        {
            _rate = await _fixerIOService.ReadRate();

            var viewModel = await _productBL.GetAllProductsInCategory(productCategoryId);
            await SetPriceInViewModels(viewModel);

            if (string.IsNullOrEmpty(searchTerm))
                return View(viewModel);

            var searchResult = _productBL.SearchItemsByName(searchTerm, viewModel);
            return View(searchResult);
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            ProductViewModel viewModel = await _productBL.GetSingleProductViewModel(id);

            if (viewModel == null)
                return NotFound();

            await SetPriceInViewModel(viewModel);
            return View(viewModel);
        }

        // GET: Products/Create
        [Authorize]
        //[Authorize(Roles ="Admin")]
        public IActionResult Create()
        {
            ViewData["ProductCategoryId"] = new SelectList(_productBL.ProductCategories, "ProductCategoryId", "ProductCategoryName");
            return View();
        }

        // POST: Products/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(ProductViewModel product)
        {
            if (ModelState.IsValid)
            {
                await _productBL.CreateProduct(product);
                return RedirectToAction("Index");
            }
            ViewData["ProductCategoryId"] = new SelectList(_productBL.ProductCategories, "ProductCategoryId", "ProductCategoryName", product.ProductCategoryId);
            return View(product);
        }

        // GET: Products/Edit/5
        [Authorize]
        //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            ProductViewModel viewModel = await _productBL.GetSingleProductViewModel(id);

            if (viewModel == null)
                return NotFound();

            await SetPriceInViewModel(viewModel);
            ViewData["ProductCategoryId"] = new SelectList(_productBL.ProductCategories, "ProductCategoryId", "ProductCategoryName", viewModel.ProductCategoryId);
            return View(viewModel);
        }

        // POST: Products/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("ProductId,ProductCategoryId,ProductDescription,ProductName,ProductPrice,ProductImageUrl")] ProductViewModel product)
        {
            if (id != product.ProductId)
                return NotFound($"id is {id}, product.ProductId is {product.ProductCategoryId}");

            if (ModelState.IsValid)
            {
                try
                {
                    await _productBL.Update(product);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.ProductId))
                        return NotFound(id);
                    else
                        throw;
                }
                return RedirectToAction("Index");
            }
            ViewData["ProductCategoryId"] = new SelectList(_productBL.ProductCategories, "ProductCategoryId", "ProductCategoryName", product.ProductCategoryId);
            return View(product);
        }

        // GET: Products/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var product = await _productBL.GetSingleProductViewModel(id);

            if (product == null)
                return NotFound();

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _productBL.RemoveProduct(id);
            return RedirectToAction("Index");
        }

        private bool ProductExists(int id)
        {
            return _productBL.ProductExists(id);
        }
        private async Task SetPriceInViewModel(ProductViewModel viewModel)
        {
            var product = await _productBL.GetSingleModelItem(viewModel.ProductId);
            _rate = await _fixerIOService.ReadRate();
            viewModel.ProductPrice = _rate * product.ProductPrice;
        }
        private async Task SetPriceInViewModels(IEnumerable<ProductViewModel> viewModels)
        {
            _rate = await _fixerIOService.ReadRate();
            foreach (var vm in viewModels)
            {
                vm.ProductPrice = vm.ProductPrice * _rate;
            }
        }
    }
}
