using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebShop.Models.ShopModels;

namespace WebShop.Controllers.WebShopControllers
{
    [Produces("application/json")]
    [Route("api/ProductTranslationsApi")]
    public class ProductTranslationsApiController : Controller
    {
        private readonly WebShopRepository _context;

        public ProductTranslationsApiController(WebShopRepository context)
        {
            _context = context;
        }

        // GET: api/ProductTranslationsApi
        [HttpGet]
        public IEnumerable<ProductTranslation> GetProductTranslations()
        {
            return _context.ProductTranslations;
        }

        // GET: api/ProductTranslationsApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductTranslation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ProductTranslation productTranslation = await _context.ProductTranslations.SingleOrDefaultAsync(m => m.ProductId == id);

            if (productTranslation == null)
            {
                return NotFound();
            }

            return Ok(productTranslation);
        }

        // PUT: api/ProductTranslationsApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductTranslation([FromRoute] int id, [FromBody] ProductTranslation productTranslation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productTranslation.ProductId)
            {
                return BadRequest();
            }

            _context.Entry(productTranslation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductTranslationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductTranslationsApi
        [HttpPost]
        public async Task<IActionResult> PostProductTranslation([FromBody] ProductTranslation productTranslation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ProductTranslations.Add(productTranslation);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProductTranslationExists(productTranslation.ProductId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetProductTranslation", new { id = productTranslation.ProductId }, productTranslation);
        }

        // DELETE: api/ProductTranslationsApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductTranslation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ProductTranslation productTranslation = await _context.ProductTranslations.SingleOrDefaultAsync(m => m.ProductId == id);
            if (productTranslation == null)
            {
                return NotFound();
            }

            _context.ProductTranslations.Remove(productTranslation);
            await _context.SaveChangesAsync();

            return Ok(productTranslation);
        }

        private bool ProductTranslationExists(int id)
        {
            return _context.ProductTranslations.Any(e => e.ProductId == id);
        }
    }
}