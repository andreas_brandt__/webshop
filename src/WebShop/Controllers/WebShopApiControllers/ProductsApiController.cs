using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebShop.Models.ShopModels;
using WebShop.BLL;

namespace WebShop.Controllers.WebShopControllers
{
    [Produces("application/json")]
    [Route("api/ProductsApi")]
    public class ProductsApiController : Controller
    {
        private readonly WebShopRepository _context;
        private ProductsBusinessLayer _productsBL;

        public ProductsApiController(WebShopRepository context, ProductsBusinessLayer productsBL)
        {
            _context = context;
            _productsBL = productsBL;
        }

        // GET: api/ProductsApi
        [HttpGet]
        public IEnumerable<Product> GetProducts()
        {
            return _productsBL.GetAllItemsIncludeAll();
        }

        // GET: api/ProductsApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Product product = await _productsBL.GetItemIncludeAll(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // PUT: api/ProductsApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct([FromRoute] int id, [FromBody] Product product)
        {
            foreach(var pt in product.Translations)
                pt.ProductId = product.ProductId;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (id != product.ProductId)
                return BadRequest();

            product.ProductCategory = await _productsBL.GetProductCategory(product.ProductCategoryId);
            
            try
            {
                await _productsBL.UpdateItem(product);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(await _productsBL.ProductExists(id)))
                    return NotFound();
                else
                    throw;
            }

            return NoContent();
        }

        // POST: api/ProductsApi
        [HttpPost]
        public async Task<IActionResult> PostProduct([FromBody] Product product)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                await _productsBL.CreateItem(product);
            }
            catch (DbUpdateException)
            {
                if (!(await _productsBL.ProductExists(product.ProductId)))
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                else
                    throw;
            }

            return CreatedAtAction("GetProduct", new { id = product.ProductId }, product);
        }

        // DELETE: api/ProductsApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Product product = await _productsBL.GetItem(id);

            if (product == null)
                return NotFound();

            await _productsBL.DeleteItem(id);
            return Ok(product);
        }
    }
}