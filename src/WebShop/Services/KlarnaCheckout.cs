﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using WebShop.BLL;

namespace WebShop.Services
{
    // Simple test with lots of hardcoded stuff
    public class KlarnaCheckout
    {
        private readonly ProductsBusinessLayer _productsBL;
        private static readonly HttpClient _client = new HttpClient();

        private static readonly string _getAuth = CreateAuthorization();
        private const string _apiAddress = "https://checkout.testdrive.klarna.com/checkout/orders";

        public KlarnaCheckout(ProductsBusinessLayer productsBL)
        {
            _productsBL = productsBL;
        }

        public async Task<string> CheckoutCart(CartBL cartBL)
        {
            Dictionary<string, object> data = await BuildData(cartBL);
            HttpRequestMessage reqMessage = BuildPostRequestMessage(data);
            var response = _client.SendAsync(reqMessage).Result;

            if (response.StatusCode == System.Net.HttpStatusCode.Created)
            {
                var location = response.Headers.Location.AbsoluteUri;
                HttpRequestMessage getMessage = BuildGetRequestMessage(location);

                var getResponse = _client.SendAsync(getMessage).Result;
                var getResponseBody = getResponse.Content.ReadAsStringAsync().Result;

                JObject jObject = JObject.Parse(getResponseBody);
                var snippet = jObject["gui"]["snippet"].ToString();

                return snippet;
            }
            return response.StatusCode.ToString(); ;
        }

        public string OrderConfirmation(string klarnaOrderId)
        {
            HttpRequestMessage getMessage = BuildGetRequestMessage(_apiAddress + "/" + klarnaOrderId);

            var getResponse = _client.SendAsync(getMessage).Result;

            if (getResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var getResponseBody = getResponse.Content.ReadAsStringAsync().Result;
                JObject jObject = JObject.Parse(getResponseBody);
                var snippet = jObject["gui"]["snippet"].ToString();
                return snippet;
            }
            return getResponse.StatusCode.ToString();
        }

        private static HttpRequestMessage BuildGetRequestMessage(string location)
        {
            HttpRequestMessage getMessage = new HttpRequestMessage(HttpMethod.Get, location);
            getMessage.Headers.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.klarna.checkout.aggregated-order-v2+json"));
            getMessage.Headers.Authorization = new AuthenticationHeaderValue("Klarna", _getAuth);

            return getMessage;
        }

        private static HttpRequestMessage BuildPostRequestMessage(Dictionary<string, object> data)
        {
            string dataAsString = JsonConvert.SerializeObject(data);
            string auth = CreateAuthorization(dataAsString);

            HttpRequestMessage reqMessage = new HttpRequestMessage(HttpMethod.Post, _apiAddress);
            reqMessage.Headers.Authorization = new AuthenticationHeaderValue("Klarna", auth);

            reqMessage.Headers.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.klarna.checkout.aggregated-order-v2+json"));

            reqMessage.Content = new StringContent(
                JsonConvert.SerializeObject(data), Encoding.UTF8, "application/vnd.klarna.checkout.aggregated-order-v2+json");

            return reqMessage;
        }

        private async Task<Dictionary<string, object>> BuildData(CartBL cartBL)
        {
            var klarnaCartItems = new List<Dictionary<string, object>>();

            foreach (var line in await cartBL.GetCartItems())
            {
                klarnaCartItems.Add(
                    new Dictionary<string, object>
                    {
                        { "reference", line.ProductId.ToString() },
                        { "name", _productsBL.GetLocalizedProductName(line.Product) },
                        { "quantity", line.Quantity },
                        { "unit_price", (int)(line.Product.ProductPrice*100) },
                        { "discount_rate", 0 },
                        { "tax_rate", 2500 }
                    });
            }

            klarnaCartItems.Add(
                new Dictionary<string, object>
                {
                    { "type", "shipping_fee" },
                    { "reference", "SHIPPING" },
                    { "name", "Shipping Fee" },
                    { "quantity", 1 },
                    { "unit_price", 4900 },
                    { "tax_rate", 2500 }
                });

            var cart = new Dictionary<string, object> { { "items", klarnaCartItems } };
            var data = new Dictionary<string, object> { { "cart", cart } };


            var merchant =
                new Dictionary<string, object>
                {
                    { "id", "5160" },
                    { "back_to_store_uri", "https://localhost:44363" },
                    { "terms_uri", "https://localhost:44363/Terms" },
                    {
                        "checkout_uri",
                        "https://localhost:44363/Cart/CheckoutOrder"
                    },

                    {
                        "confirmation_uri",
                        "https://localhost:44363/Cart/OrderConfirmation"+
                        "?klarna_order_id={checkout.order.id}"
                    },

                    {
                        "push_uri",
                        "https://example.com/push.aspx" +
                        "?klarna_order_id={checkout.order.id}"
                    }
                };
            //CultureInfo.CurrentCulture.
            data.Add("purchase_country", "SE");
            data.Add("purchase_currency", "SEK");
            data.Add("locale", "sv-se");
            data.Add("merchant", merchant);
            return data;
        }

        //base64(hex(sha256 (request_payload + shared_secret)))
        private static string CreateAuthorization(string payLoad = "")
        {
            string sharedSecret = "tE94QeKzSdUVJGe";

            using (var algorithm = SHA256.Create())
            {
                var hash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(payLoad + sharedSecret));
                var base64 = System.Convert.ToBase64String(hash);
                return base64;
            }
        }
    }
}
