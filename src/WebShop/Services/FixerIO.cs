﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace WebShop.Services
{
    public class FixerIO : IFixerIO
    {
        private static HttpClient _client = new HttpClient();

        public static readonly string BaseCurrency = "SEK";

        public FixerIO() { }

        /// <summary>
        /// Reads the currency rate off the fixer.io api
        /// Base currency is fixed and current currency is retreived from RegioInfo
        /// </summary>
        public async Task<decimal> ReadRate()
        {
            string currencyName = new RegionInfo(CultureInfo.CurrentCulture.Name).ISOCurrencySymbol;

            // Crazy api doesn't return anything when base and symbol is the same
            if (currencyName == BaseCurrency)
                return 1.0m;

            string requestUri = "http://api.fixer.io/latest?base=" + BaseCurrency + ";symbols=" + currencyName;
            var response = _client.GetAsync(requestUri).Result;
            var result = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                JObject obj = JObject.Parse(result);
                return (decimal)obj["rates"][currencyName];
            }
            return -1m;
        }
    }
}
