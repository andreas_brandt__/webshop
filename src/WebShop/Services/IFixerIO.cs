﻿using System.Threading.Tasks;

namespace WebShop.Services
{
    public interface IFixerIO
    {
        Task<decimal> ReadRate();
    }
}
