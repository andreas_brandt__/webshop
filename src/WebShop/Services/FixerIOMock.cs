﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebShop.Services
{
    public class FixerIOMock : IFixerIO
    {
        public Task<decimal> ReadRate()
        {
            return Task.FromResult(1.0m);
        }
    }
}
