﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models.ShopModels;

namespace WebShop.ViewModels
{
    public class CartLinesViewModel
    {
        //public List<CartLine> CartLines { get; set; }
        public List<CartLineViewModel> CartLines { get;set; }
        [DataType(DataType.Currency)]
        public decimal TotalPrice { get; set; }
        public int Quantity { get; set; }
    }
}
