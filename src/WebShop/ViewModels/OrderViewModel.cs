﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebShop.ViewModels
{
    public class OrderViewModel
    {
        public decimal TotalPriceExcludingTax { get; set; }
        public decimal TotalPriceIncludingTax { get; set; }
        public decimal TotalTax { get; set; }

    }

    public class OrderItemViewModel
    {
        public string reference { get; set; }
        public string name { get; set; }
        public int quantity { get; set; }

    }
}
