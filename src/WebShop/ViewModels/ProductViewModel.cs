﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models.ShopModels;

namespace WebShop.ViewModels
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public int ProductCategoryId { get; set; }

        public ProductCategory ProductCategory { get; set; }

        [Display(Name = "ProductName", ResourceType = typeof(WebShop.Resources.Localization.Models.ShopModels.Product))] // , ResourceType = typeof(Resources.Localization.Models.ShopModels.Product)
        [MaxLength(30, ErrorMessage = "Max 30 characters long name")]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Product name is required")]
        public string ProductName { get; set; }

        [Display(Name = "Description", ResourceType = typeof(WebShop.Resources.Localization.Models.ShopModels.Product))]
        [DataType(DataType.Text)]
        [MaxLength(100, ErrorMessage = "Max 100 characters long name")]
        //[RegularExpression(@"^[a-zåäöA-ZÅÄÖ\s]{1,40}$", ErrorMessage = "Only characters are allowed")]
        public string ProductDescription { get; set; }

        [Display(Name = "Price", ResourceType = typeof(WebShop.Resources.Localization.Models.ShopModels.Product))]
        [DataType(DataType.Currency)]
        [Range(0.1, 1000000, ErrorMessage = "Must be between 0.1 and 1000000")]
        [Required(ErrorMessage = "Price is required")]
        public decimal ProductPrice { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ProductImageUrl { get; set; }
    }
}
