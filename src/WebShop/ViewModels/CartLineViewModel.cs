﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models.ShopModels;

namespace WebShop.ViewModels
{
    public class CartLineViewModel
    {
        public CartLine CartLine { get; set; }

        public string ProductName { get; set; }

        //public int Quantity { get; set; }

        [DataType(DataType.Currency)]
        public decimal ProductPrice { get; set; }

        [DataType(DataType.Currency)]
        public decimal LinePriceSum { get; set; }
    }
}
