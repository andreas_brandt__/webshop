﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebShop.BLL;
using WebShop.Models.ShopModels;

namespace WebShop.ViewComponents
{
    public class CategorySelectViewComponent : ViewComponent
    {
        private ProductsBusinessLayer _productsBL;
        public CategorySelectViewComponent(ProductsBusinessLayer productsBL)
        {
            _productsBL = productsBL;
        }   
        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<ProductCategory> categories =  await _productsBL.GetAllCategories();
            return View(categories);
        }
    }
}
