﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebShop.BLL;
using WebShop.Models.ShopModels;
using WebShop.Services;
using WebShop.ViewModels;

namespace WebShop.ViewComponents
{
    public class CartViewComponent : ViewComponent
    {
        private ProductsBusinessLayer _productsBL;
        private IFixerIO _fixerIOService;

        public CartViewComponent(ProductsBusinessLayer productsBL, IFixerIO fixerIOService)
        {
            _fixerIOService = fixerIOService;
            _productsBL = productsBL;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            decimal rate = await _fixerIOService.ReadRate();

            var cart = CartBL.GetCart(this.HttpContext);
            var count = await cart.GetCount();
            var priceSum = await cart.GetTotal();
            var cartLines = await cart.GetCartItems();
            
            List<CartLineViewModel> cartLineViewModel = new List<CartLineViewModel>();
            foreach (var cartLine in cartLines)
            {
                cartLineViewModel.Add(new CartLineViewModel
                {
                    CartLine = cartLine,
                    ProductName = _productsBL.GetLocalizedProductName(cartLine.Product),
                    LinePriceSum = cartLine.Product.ProductPrice * rate * cartLine.Quantity,
                    ProductPrice = cartLine.Product.ProductPrice * rate
                    
                });
            }
            CartLinesViewModel viewModel = new CartLinesViewModel
            {
                Quantity = count,
                TotalPrice = priceSum,
                CartLines = cartLineViewModel
            };

            return View(viewModel);
        }
    }
}
