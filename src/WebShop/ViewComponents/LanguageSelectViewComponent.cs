﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebShop.ViewComponents
{
    public class LanguageSelectViewComponent : ViewComponent
    {
        private IOptions<RequestLocalizationOptions> _locOptions;

        public LanguageSelectViewComponent(IOptions<RequestLocalizationOptions> locOptions)
        {
            _locOptions = locOptions;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var cultureItems = await _locOptions.Value.SupportedUICultures
                .Select(c => new SelectListItem { Value = c.Name, Text = c.DisplayName })
                .ToAsyncEnumerable().ToList();

            return View((List<SelectListItem>)cultureItems);
        }



    }
}
