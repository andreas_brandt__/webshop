﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models.ShopModels;

namespace WebShop.BLL
{
    public class ProductsBusinessLayer : IBusinessLayerCRUD<Product>
    {
        private WebShopRepository _context;

        public ProductsBusinessLayer(WebShopRepository context)
        {
            _context = context;
        }

        public async Task CreateItem(Product item)
        {
            Product p = new Product
            {
                ProductCategoryId = item.ProductCategoryId,
                ProductImageUrl = item.ProductImageUrl,
                ProductPrice = item.ProductPrice,
                Translations = new List<ProductTranslation>()
            };

            _context.Products.Add(p);
            await _context.SaveChangesAsync();

            foreach(var translation in item.Translations)
            {
                _context.Add(
                    new ProductTranslation
                    {
                        Language = translation.Language,
                        ProductDescription = translation.ProductDescription,
                        ProductName = translation.ProductName,
                        ProductId = p.ProductId
                    });
            }
            //item.Translations.ToList().ForEach(t => _context.Add(t));
            await _context.SaveChangesAsync();
        }

        internal async Task<List<ProductCategory>> GetAllCategories()
        {
            return await _context.ProductCategories.ToListAsync();
        }

        public async Task DeleteItem(int id)
        {
            var product = await _context.Products.SingleOrDefaultAsync(m => m.ProductId == id);
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Product>> GetAllItems()
        {
            return _context.Products;
        }

        public string GetLocalizedProductName(Product product)
        {
            var translations = product.Translations
                .Where(t => t.Language == CultureInfo.CurrentCulture.TwoLetterISOLanguageName);

            // Return translation if it was found
            if (translations.Count() == 1)
                return translations.First().ProductName;

            return string.Empty;
        }

        public IEnumerable<Product> GetAllItemsIncludeAll()
        {
            return _context.Products
                .Include(p => p.Translations)
                .Include(p => p.ProductCategory);
        }

        public async Task<Product> GetItem(int? id)
        {
            var product = await _context.Products.SingleOrDefaultAsync(p => p.ProductId == id);
            return product;
        }
        public async Task<Product> GetItemIncludeAll(int id)
        {
            return await _context.Products
                .Include(p => p.Translations)
                .Include(p => p.ProductCategory)
                .SingleOrDefaultAsync(p => p.ProductId == id);
        }

        public async Task<ProductCategory> GetProductCategory(int productCategoryId)
        {
            return await _context.ProductCategories.SingleOrDefaultAsync(c => c.ProductCategoryId == productCategoryId);
        }

        public async Task UpdateItem(Product item)
        {
            _context.Entry(item).State = EntityState.Modified;
            foreach(var t in item.Translations)
                _context.Entry(t).State = EntityState.Modified;
            _context.Entry(item.ProductCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

   

        public async Task AddProductTranslation(int productId, ProductTranslation translation)
        {
            if(await ProductExists(productId))
                _context.ProductTranslations.Add(translation);
        }

        public async Task<bool> ProductExists(int id)
        {
            return await _context.Products.AnyAsync(e => e.ProductId == id);
        }

    }
}
