﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebShop.BLL
{
    // Interface for implementations of business layer logic
    // Typically connects to some database/datasource
    // CRUD-operations
    public interface IModelToViewModelBusinessLogic<TModel, TViewModel>
    {
        Task<TViewModel> GetSingleViewModelItem(int? id);
        Task<TModel> GetSingleModelItem(int? id);
        Task<IEnumerable<TViewModel>> GetAllViewModeltems();

        Task CreateItem(TViewModel viewModel);

        Task DeleteItem(int id);

        Task UpdateItem(TViewModel viewModel); 
    }
}
