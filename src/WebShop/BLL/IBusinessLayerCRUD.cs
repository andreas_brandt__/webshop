﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebShop.BLL
{
    interface IBusinessLayerCRUD<TModel>
    {
        Task CreateItem(TModel item);
        Task<TModel> GetItem(int? id);
        Task<IEnumerable<TModel>> GetAllItems();
        Task UpdateItem(TModel item);
        Task DeleteItem(int id);
    }
}
