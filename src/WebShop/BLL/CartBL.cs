﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using WebShop.BLL;
using WebShop.Models.ShopModels;
using WebShop.Services;
using WebShop.ViewModels;

namespace WebShop.BLL
{
    public class CartBL
    {
        private WebShopRepository _context;
        private IFixerIO _fixerIOService;

        public string ShoppingCartId { get; set; }

        public CartBL(WebShopRepository context, IFixerIO fixerIOService)
        {
            _fixerIOService = fixerIOService;
            _context = context;
        }

        public static CartBL GetCart(HttpContext context)
        {
            var repo = context.RequestServices.GetService(typeof(WebShopRepository)) as WebShopRepository;
            var fixer = context.RequestServices.GetService(typeof(IFixerIO)) as IFixerIO;
            var cart = new CartBL(repo, fixer);
            cart.ShoppingCartId = cart.GetCartId(context);

            return cart;
        }


        public async Task AddToCart(Product product)
        {
            var cartItem = await _context.CartLines.SingleOrDefaultAsync(
                                c => c.CartId == ShoppingCartId
                                && c.ProductId == product.ProductId);

            if(cartItem == null)
            {
                cartItem = new CartLine
                {
                    ProductId = product.ProductId,
                    Product = product,
                    Quantity = 1,
                    CartId = ShoppingCartId
                };

                _context.Add(cartItem);
            }
            else
            {
                cartItem.Quantity++;
            }
            await _context.SaveChangesAsync();
        }

        public async Task<int> RemoveOneFromCart(int id)
        {
            var cartItem = await _context.CartLines.SingleOrDefaultAsync(
                cart => cart.ProductId == id 
                && cart.CartId == ShoppingCartId);

            int itemCount = 0;

            if (cartItem != null)
            {
                if (cartItem.Quantity > 1)
                {
                    cartItem.Quantity--;
                    itemCount = cartItem.Quantity;
                }
                else
                {
                    _context.CartLines.Remove(cartItem);
                }

                await _context.SaveChangesAsync();
            }
            return itemCount;
        }

        public async Task ChangeQuantity(int id, int newQuantity)
        {
            var cartItem = _context.CartLines.SingleOrDefault(
                                cart => cart.ProductId == id
                                && cart.CartId == ShoppingCartId);

            if(newQuantity <= 0)
                _context.CartLines.Remove(cartItem);
            else
                cartItem.Quantity = newQuantity;

            await _context.SaveChangesAsync();
        }

        public async Task EmptyCart(HttpContext context)
        {
            //var cartItems = _context.CartLines.Where(
            //    cart => cart.CartId == ShoppingCartId);

            //foreach (var cartItem in cartItems)
            //    _context.CartLines.Remove(cartItem);

            //await _context.SaveChangesAsync();
            //var cart = GetCart(context);
            context.Response.Cookies.Delete("cart");
        }

        public async Task<List<CartLine>> GetCartItems()
        {
            var cartLines = _context.CartLines
                .Where(cart => cart.CartId == ShoppingCartId)
                .Include(c => c.Product)
                .Include(c => c.Product.ProductCategory)
                .Include(c => c.Product.Translations);

            return await cartLines.ToListAsync();
        }

        public async Task<int> GetCount()
        {
            // Get the count of each item in the cart and sum them up
            if (!_context.CartLines.Any())
                return 0;
            int? count = await (from cartItems in _context.CartLines
                          where cartItems.CartId == ShoppingCartId
                          select cartItems.Quantity).SumAsync();
            // Return 0 if all entries are null
            if (count == null)
                return 0;
            return (int)count;
            //return count ?? 0;
        }

        public async Task<decimal> GetTotal()
        {
            decimal? total = (from cartItems in _context.CartLines
                              where cartItems.CartId == ShoppingCartId
                              select (int?)cartItems.Quantity *
                              cartItems.Product.ProductPrice).Sum();

            decimal rate = await _fixerIOService.ReadRate();
            return total * rate ?? decimal.Zero;
        }
        
        public string GetCartId(HttpContext context)
        {
            string cartId = string.Empty;
            if (context.Request.Cookies["cart"] == null)
            {
                //if(!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                //    cartId = context.User.Identity.Name;
                //else
                    cartId = Guid.NewGuid().ToString();
                
                context.Response.Cookies.Append(
                        "cart",
                        cartId,
                        new CookieOptions
                        {
                            Path = "/",
                            HttpOnly = false,
                            Secure = false,
                            Expires = DateTime.Now.AddMonths(8)
                        }
                );
            }
            else
            { 
                cartId = HtmlEncoder.Default.Encode(context.Request.Cookies["cart"]);
            }
            return cartId;

        }

        // When a user has logged in, migrate their shopping cart to
        // be associated with their username
        public async Task MigrateCart(string userName)
        {
            var shoppingCart = _context.CartLines.Where(
                c => c.CartId == ShoppingCartId);

            foreach (CartLine item in shoppingCart)
            {
                item.CartId = userName;
            }
            await _context.SaveChangesAsync();
        }
    }
}
