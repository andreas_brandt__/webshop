﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebShop.Models.ShopModels;
using WebShop.Services;
using WebShop.ViewModels;

namespace WebShop.BLL
{
    public class ProductBL// : IModelToViewModelBusinessLogic<Product, ProductViewModel>
    {
        private WebShopRepository _context;
        private IFixerIO _fixerIOService;
        public ProductBL(WebShopRepository context, IFixerIO fixerIOService)
        {
            _context = context;
            _fixerIOService = fixerIOService;

        }
        public async Task<ProductViewModel> GetSingleProductViewModel(int? id)
        {
            var product = await _context.Products.Include(p => p.ProductCategory).SingleOrDefaultAsync(m => m.ProductId == id);
            if (product != null)
            {
                var productTranslation =
                    await _context.ProductTranslations.SingleOrDefaultAsync(x =>
                        x.Language == CultureInfo.CurrentCulture.TwoLetterISOLanguageName
                        && x.ProductId == product.ProductId);

                ProductViewModel viewModel = new ProductViewModel
                {
                    ProductId = product.ProductId,
                    ProductCategory = product.ProductCategory,
                    ProductCategoryId = product.ProductCategoryId,
                    ProductDescription = productTranslation?.ProductDescription,
                    ProductName = productTranslation?.ProductName,
                    ProductPrice = product.ProductPrice,
                    ProductImageUrl = product.ProductImageUrl
                };


                return viewModel;
            }
            return null;
        }

        public async Task<IEnumerable<ProductViewModel>> GetAllProductsInCategory(int? productCategoryId)
        {
            var products = await GetAllProducts();
            if (productCategoryId == null)
                return products;
            var productInCategory = products.Where(p => p.ProductCategoryId == productCategoryId);
            return productInCategory;
        }

        public async Task<IEnumerable<ProductViewModel>> GetAllProducts()
        {
            var query = from p in _context.Products
                        join pt in _context.ProductTranslations on
                        new { p.ProductId, Second = CultureInfo.CurrentCulture.TwoLetterISOLanguageName }
                        equals new { pt.ProductId, Second = pt.Language }
                        select new ProductViewModel
                        {
                            ProductId = p.ProductId,
                            ProductCategory = p.ProductCategory,
                            ProductCategoryId = p.ProductCategoryId,
                            ProductPrice = p.ProductPrice,
                            ProductDescription = pt.ProductDescription,
                            ProductName = pt.ProductName,
                            ProductImageUrl = p.ProductImageUrl
                        };

            var viewModelList = await query.ToListAsync();
            foreach (var p in _context.Products)
            {
                if(query.Where(pv => pv.ProductId == p.ProductId).Count() < 1)
                {
                    viewModelList.Add(new ProductViewModel
                    {
                        ProductId = p.ProductId,
                        ProductCategory = p.ProductCategory,
                        ProductCategoryId = p.ProductCategoryId,
                        ProductPrice = p.ProductPrice,
                        ProductDescription = string.Empty,
                        ProductName = string.Empty,
                        ProductImageUrl = p.ProductImageUrl
                    });
                }
            }
            //var newQuery = from p in _context.Products
            //               join pt in _context.ProductTranslations on
            //               new { };

            return viewModelList;
            //return await query.ToListAsync();
        }

        public IEnumerable<ProductViewModel> SearchItemsByName(string searchTerm, IEnumerable<ProductViewModel> viewModel)
        {
            var searchResult = viewModel.Where(
                p => p.ProductName.ToLower().Contains(searchTerm.ToLower()));
            return searchResult;
        }
        public IEnumerable<ProductViewModel> ItemsByCategory(ProductCategory category, IEnumerable<ProductViewModel> viewModel)
        {
            var searchResult = viewModel.Where(p => p.ProductCategory == category);
            return searchResult;
        }
        public async Task<Product> ProductById(int? id)
        {
            return await _context.Products.SingleOrDefaultAsync(m => m.ProductId == id);
        }

        public async Task CreateProduct(Product product)
        {
            _context.Add(product);
            await _context.SaveChangesAsync();
        }

        public async Task CreateProduct(ProductViewModel productViewModel)
        {
            var product = new Product
            {
                ProductCategory = productViewModel.ProductCategory,
                ProductCategoryId = productViewModel.ProductCategoryId,
                ProductPrice = productViewModel.ProductPrice,
                ProductImageUrl = productViewModel.ProductImageUrl
            };

            _context.Add(product);
            await _context.SaveChangesAsync();

            var productTranslation = new ProductTranslation
            {
                Language = CultureInfo.CurrentCulture.TwoLetterISOLanguageName,
                ProductDescription = productViewModel.ProductDescription,
                ProductId = product.ProductId,
                ProductName = productViewModel.ProductName
            };
            _context.Add(productTranslation);
            await _context.SaveChangesAsync();
        }
        
        public async Task RemoveProduct(int id)
        {
            var product = await _context.Products.SingleOrDefaultAsync(m => m.ProductId == id);
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
        }

        public DbSet<ProductCategory> ProductCategories { get { return _context.ProductCategories; } }
        public DbSet<Product> Products { get { return _context.Products; } }

        public async Task Update(ProductViewModel productViewModel)
        {
            decimal rate = await _fixerIOService.ReadRate();
            var product = await ProductById(productViewModel.ProductId);

            product.ProductCategory = productViewModel.ProductCategory;
            product.ProductCategoryId = productViewModel.ProductCategoryId;
            product.ProductImageUrl = productViewModel.ProductImageUrl;
            product.ProductPrice = productViewModel.ProductPrice / rate;
            
            _context.Update(product);
            await _context.SaveChangesAsync();

            ProductTranslation productTranslation = await ProductTranslationByIdAndLanguage(productViewModel.ProductId, CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
            if(productTranslation == null)
            {
                productTranslation = new ProductTranslation
                {
                    ProductDescription = productViewModel.ProductDescription,
                    ProductName = productViewModel.ProductName,
                    Language = CultureInfo.CurrentCulture.TwoLetterISOLanguageName,
                    ProductId = product.ProductId
                };
                _context.ProductTranslations.Add(productTranslation);
            }
            else
            { 
                productTranslation.ProductDescription = productViewModel.ProductDescription;
                productTranslation.ProductName = productViewModel.ProductName;
                _context.Update(productTranslation);
            }

            await _context.SaveChangesAsync();
        }

        private async Task<ProductTranslation> ProductTranslationByIdAndLanguage(int productId, string twoLetterISOLanguageName)
        {
            var productTranslation =
                await _context.ProductTranslations.SingleOrDefaultAsync(x =>
                    x.Language == CultureInfo.CurrentCulture.TwoLetterISOLanguageName
                    && x.ProductId == productId);
            return productTranslation;
        }

        public bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.ProductId == id);
        }

        private Product ProductViewModelToProduct(ProductViewModel productViewModel)
        {
            return new Product
            {
                ProductCategory = productViewModel.ProductCategory,
                ProductCategoryId = productViewModel.ProductCategoryId,
                ProductId = productViewModel.ProductId,
                ProductImageUrl = productViewModel.ProductImageUrl,
                ProductPrice = productViewModel.ProductPrice,
               // Translations = _context.ProductTranslations.
            };
        }

        public async Task<ProductViewModel> GetSingleViewModelItem(int? id)
        {
            var product = await _context.Products.SingleOrDefaultAsync(m => m.ProductId == id);
            var productTranslation =
                await _context.ProductTranslations.SingleOrDefaultAsync(x =>
                    x.Language == CultureInfo.CurrentCulture.TwoLetterISOLanguageName
                    && x.ProductId == product.ProductId);

            if ((product == null || productTranslation == null))
                return null;

            ProductViewModel viewModel = new ProductViewModel
            {
                ProductId = product.ProductId,
                ProductCategory = product.ProductCategory,
                ProductCategoryId = product.ProductCategoryId,
                ProductDescription = productTranslation.ProductDescription,
                ProductName = productTranslation.ProductName,
                ProductPrice = product.ProductPrice,
                ProductImageUrl = product.ProductImageUrl
            };
            return viewModel;
        }

        public async Task<Product> GetSingleModelItem(int? id)
        {
            return await _context.Products.SingleOrDefaultAsync(m => m.ProductId == id);
        }

        public async Task<IEnumerable<ProductViewModel>> GetAllViewModeltems()
        {
            var query = from p in _context.Products
                        join pt in _context.ProductTranslations on
                        new { p.ProductId, Second = CultureInfo.CurrentCulture.TwoLetterISOLanguageName }
                        equals new { pt.ProductId, Second = pt.Language }
                        select new ProductViewModel
                        {
                            ProductId = p.ProductId,
                            ProductCategory = p.ProductCategory,
                            ProductCategoryId = p.ProductCategoryId,
                            ProductPrice = p.ProductPrice,
                            ProductDescription = pt.ProductDescription,
                            ProductName = pt.ProductName,
                            ProductImageUrl = p.ProductImageUrl
                        };
            return await query.ToListAsync();
        }



        public Task CreateItem(ProductViewModel viewModel)
        {
            throw new NotImplementedException();
        }

        public Task DeleteItem(int id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateItem(ProductViewModel viewModel)
        {
            throw new NotImplementedException();
        }
    }
}
